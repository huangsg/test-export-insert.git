/* 查询或快速造数据 */
select count(*)
from tb_user;
select *
from tb_user
order by id desc limit 5;
insert into tb_user(user_no, name, real_name, depart, sex, role_type, age, email, data_json, is_delete, version,
                    create_time, create_by, update_time, update_by)
select user_no,
       name,
       real_name,
       depart,
       sex,
       role_type,
       age,
       email,
       data_json,
       is_delete,
       version,
       create_time,
       create_by,
       update_time,
       update_by
from tb_user;