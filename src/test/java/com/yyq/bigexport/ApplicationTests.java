package com.yyq.bigexport;

import cn.hutool.core.util.RandomUtil;
import com.yyq.bigexport.entity.User;
import com.yyq.bigexport.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
@SpringBootTest
class ApplicationTests {

    @Autowired
    UserService userService;

    @Test
    void dataAddUser() {
        long time1 = System.currentTimeMillis();
        List<User> userList = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            Date date = new Date();
            User user = new User();
            //user.setId(Long.valueOf(i + 1));
            user.setName(RandomUtil.randomString(RandomUtil.BASE_CHAR_NUMBER, 6));
            user.setAge(RandomUtil.randomInt());
            user.setVersion(1);
            user.setCreateBy(1);
            user.setEmail(user.getAge() + "@qq.com");

            userList.add(user);
        }
        userService.saveBatch(userList);
        //userService.updateBatchById(userList);
        long time2 = System.currentTimeMillis();
        System.out.println(String.format("新增记录数：%d条,耗时%d", userList.size(), time2 - time1));
    }
}
