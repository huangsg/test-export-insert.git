package com.yyq.bigexport.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yyq.bigexport.dao.OrderDao;
import com.yyq.bigexport.dao.UserDao;
import com.yyq.bigexport.entity.Order;
import com.yyq.bigexport.entity.User;
import com.yyq.bigexport.service.OrderService;
import com.yyq.bigexport.service.UserService;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl extends ServiceImpl<OrderDao, Order> implements OrderService {
    @Override
    public IPage<T> pageList(Integer pageIndex, Integer pageSize, LambdaQueryWrapper queryWrapper) {
        IPage page = new Page(pageIndex, pageSize);
        return page(page, queryWrapper);
    }
}
