package com.yyq.bigexport.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;

public interface ExportService {
    //分页查询
    IPage pageList(Integer pageIndex, Integer pageSize, LambdaQueryWrapper queryWrapper);
}
