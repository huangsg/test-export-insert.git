package com.yyq.bigexport.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yyq.bigexport.entity.User;

public interface UserService extends IService<User>, ExportService {
}
