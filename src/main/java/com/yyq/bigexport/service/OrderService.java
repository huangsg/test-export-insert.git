package com.yyq.bigexport.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yyq.bigexport.entity.Order;
import com.yyq.bigexport.entity.User;

public interface OrderService extends IService<Order>, ExportService {
}
