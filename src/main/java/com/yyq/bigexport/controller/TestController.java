package com.yyq.bigexport.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yyq.bigexport.entity.Order;
import com.yyq.bigexport.entity.User;
import com.yyq.bigexport.service.OrderService;
import com.yyq.bigexport.service.UserService;
import com.yyq.bigexport.util.export.ExcelExportUtils;
import com.yyq.bigexport.util.insert.InsertUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class TestController {

    @Autowired
    private UserService userService;

    @Autowired
    private OrderService orderService;

    @GetMapping("/test/export/user")
    public String exportUser() {
        //查询条件
        LambdaQueryWrapper queryWrapper = new LambdaQueryWrapper<User>().ge(User::getUser_id, 2000000);
        return ExcelExportUtils.export(userService, queryWrapper, User.class);
    }

    /**
     * 插入user数据测试
     * @return
     */
    @GetMapping("/test/insert/user/{totalCount}")
    public void insertUser(@PathVariable("totalCount") Integer totalCount) {
        InsertUtils.insert(userService, totalCount);
    }
    @GetMapping("/test/export/order")
    public String exportOrder() {
        //查询条件
        LambdaQueryWrapper queryWrapper = new LambdaQueryWrapper<Order>().ge(Order::getId, 0);
        return ExcelExportUtils.export(orderService, queryWrapper, Order.class);
    }

    @GetMapping("/test/pagelist-user")
    public IPage<User> pagelistUser(Integer pageIndex, Integer pageSize) {
        return userService.pageList(pageIndex, pageSize, null);
    }
}
