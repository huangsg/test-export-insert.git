package com.yyq.bigexport.util;

import cn.hutool.crypto.digest.MD5;
import com.github.javafaker.Faker;
import com.yyq.bigexport.entity.User;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class UserGenerator {

    private static final int THREAD_POOL_SIZE = 5; // 线程池大小，根据需要调整
    private static final int USERS_PER_THREAD = 10000;

    /**
     * 生成随机用户信息
     */
    public static List<User> generateRandomUser(int count) {
        Faker faker = new Faker();


        return Stream.generate(() -> {
                    User user = new User();
                    user.setDeptId(faker.number().randomNumber());
                    user.setLoginName(faker.name().username());
                    user.setUserName(faker.name().fullName());
                    user.setUserType(faker.options().option("00", "01"));
                    user.setEmail(faker.internet().emailAddress());
                    user.setPhoneNumber(faker.phoneNumber().cellPhone());
                    user.setSex(faker.options().option("0", "1", "2"));
                    user.setAvatar(faker.internet().avatar());
                    user.setPassword(MD5.create().digestHex16(faker.internet().password()));
                    user.setSalt(faker.internet().password());
                    user.setStatus(faker.options().option("0", "1"));
                    user.setDelFlag(faker.options().option("0", "2"));
                    user.setLoginIp(faker.internet().ipV4Address());
                    user.setLoginDate(new Date());
                    user.setPwdUpdateDate(new Date());
                    user.setCreateBy(faker.name().fullName());
                    user.setCreateTime(new Date());
                    user.setUpdateBy(faker.name().fullName());
                    user.setUpdateTime(new Date());
                    user.setRemark(faker.lorem().sentence());
                    return user;
                })
                .limit(count)
                .parallel()  // 将流转为并行流
                .collect(Collectors.toList());
    }
    public static List<User> generateRandomUsersMultithreaded(int count, int threads) throws InterruptedException, ExecutionException {

        ExecutorService executorService = Executors.newFixedThreadPool(threads);

        List<Future<List<User>>> futures = new ArrayList<>();

        for (int i = 0; i < threads; i++) {
            Future<List<User>> future = executorService.submit(() -> generateRandomUser(count));
            futures.add(future);
        }

        List<User> allUsers = new ArrayList<>();
        for (Future<List<User>> future : futures) {
            allUsers.addAll(future.get());
        }

        executorService.shutdown();
        executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);

        return allUsers;
    }

    /**
     * 生成指定总用户数的用户信息，并打印统计信息
     *
     * @param totalCount 总用户数
     * @throws InterruptedException 打断异常
     * @throws ExecutionException   执行异常
     */
    public static List<User> generateUsersAndPrintStats(int totalCount) throws InterruptedException, ExecutionException {
        log.info("生成用户  开始,总数量 totalCount={}",totalCount);
        List<User> allUsers = new LinkedList<>();
        ExecutorService executorService = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
        ExecutorCompletionService<List<User>> completionService = new ExecutorCompletionService<>(executorService);
        long startTime = System.currentTimeMillis();
        log.info("生成用户 每批次插入数量 USERS_PER_THREAD={}",USERS_PER_THREAD);
        try {
            // 计算循环次数
            int loopCount = (totalCount + USERS_PER_THREAD - 1) / USERS_PER_THREAD;
            log.info("生成用户 循环批次 loopCount={}",loopCount);
            // 提交任务到线程池
            for (int i = 1; i <= loopCount; i++) {
                log.info("生成用户 循环批次 开始 批次={}",i);
                completionService.submit(() -> {
                    long threadStartTime = System.currentTimeMillis();
                    List<User> users = generateRandomUser(USERS_PER_THREAD);
                    long threadEndTime = System.currentTimeMillis();
                    log.info("创建用户线程"+Thread.currentThread().getId()+"，新增"+USERS_PER_THREAD+"条数据结束， 耗时"+(threadEndTime - threadStartTime));
                    return users;
                });
            }

            // 收集已完成的任务
            for (int i = 0; i < loopCount; i++) {
                Future<List<User>> future = completionService.take();
                allUsers.addAll(future.get());
            }

        } finally {
            long endTime = System.currentTimeMillis();
            System.out.println("总用户数：" + allUsers.size());
            System.out.println("总用时：" + (endTime - startTime) + " milliseconds");
            executorService.shutdown();
            return allUsers;
        }

    }

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        int totalCount = 10000; // 你的总数据条数
        generateUsersAndPrintStats(totalCount);
        long startTime = System.currentTimeMillis();
//        generateRandomUser(10000);
        long endTime = System.currentTimeMillis();
        long countTime = endTime - startTime;
        log.info("结束时间 time={}",countTime);
    }
}
