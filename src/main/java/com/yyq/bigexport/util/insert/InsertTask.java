package com.yyq.bigexport.util.insert;

import com.yyq.bigexport.entity.User;
import com.yyq.bigexport.service.UserService;
import com.yyq.bigexport.util.UserGenerator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

@Slf4j
public class InsertTask implements Runnable {
    private UserService userService;
    //每次批量插入总数量
    private int batchSize;
    //每次批量单纯插入数量10000
    private final static int insertSize = 2000;
    private CountDownLatch countDownLatch;

    public InsertTask(UserService userService, int batchSize, CountDownLatch countDownLatch) {
        this.userService = userService;
        this.batchSize = batchSize;
        this.countDownLatch = countDownLatch;
    }

    @Override
    public void run() {
        log.info("插入数据线程开始 线程名={}",Thread.currentThread().getName());
        try {
            //并发生成用户数
            List<User> records = UserGenerator.generateUsersAndPrintStats(batchSize);
            batchInsert(records);
            // 清空allUsers
            records.clear();
            // 通知gc回收内存
            System.gc();
        } catch (Exception e){
            log.error("插入数据失败", e);
        } finally{
            // 清空数据
            //计数器减一
            countDownLatch.countDown();
            // 可以添加一些资源释放等操作
            log.info("线程{}释放资源", Thread.currentThread().getName());
        }
    }


    @Transactional
    public void batchInsert(List<User> records) {
        log.info("批量插入数据，总数={}", records.size());
        int size = records.size();
        int batchCount = (size + insertSize - 1) / insertSize;
        log.info("线程={}插入数据总批次={}", Thread.currentThread().getName(),batchCount);
        int insertErrCount = 0;
        for (int i = 0; i < batchCount; i ++) {
            try {
                int endIndex = Math.min(i + insertSize, size);
                List<User> batch = records.subList(i, endIndex);
                // 插入数据
                log.info("线程{}正在插入数据{}条", Thread.currentThread().getName(), insertSize);
                userService.saveBatch(batch,insertSize);
                log.info("线程{}插入完成", Thread.currentThread().getName());
            }catch (Exception e){
                insertErrCount++;
                log.error("批量插入失败 err={}",e.getMessage(),e);
            }

        }
        log.info("插入数据完成，总数={}, 成功数={}, 失败数={}", size, size - insertErrCount, insertErrCount);
    }
}
