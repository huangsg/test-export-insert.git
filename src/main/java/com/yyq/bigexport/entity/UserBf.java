package com.yyq.bigexport.entity;


import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author
 * @since 2022-02-24
 */
@Data
@Accessors(chain = true)
@ColumnWidth(15)
public class UserBf {

    //@ApiModelProperty("主键ID")
    @TableId(value = "id", type = IdType.AUTO)
    @ExcelProperty({"主标题", "编号"})
    private Long id;

    @ExcelProperty({"主标题", "工号"})
    private String userNo;

    //@ApiModelProperty("姓名")
    @ExcelProperty("姓名")
    private String name;

    @ExcelProperty("姓名")
    private String realName;
    @ExcelProperty("部门")
    private String depart;
    @ExcelProperty("性别")
    private String sex;
    private Integer roleType;

    //@ApiModelProperty("年龄")
    private Integer age;

    //@ApiModelProperty("邮箱")
    private String email;

    //@ApiModelProperty("乐观锁")
    @TableField("version")
    //@Version
    private Integer version;
    @ExcelIgnore
    private Boolean isDelete;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime = new Date();

    private Integer createBy;

    private Date updateTime;
    private Integer updateBy;
}
