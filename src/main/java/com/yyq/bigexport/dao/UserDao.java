package com.yyq.bigexport.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yyq.bigexport.entity.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserDao extends BaseMapper<User> {

}
