package com.yyq.bigexport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @ClassName: ExportExcelDataApplication
 * @Description: 启动类
 * @Author: Deamer
 * @Date: 2022/5/3 19:01
 **/
//@EnableScheduling
@SpringBootApplication
@EnableAsync
public class EasyExportDataApplication {
    public static void main(String[] args) {
        SpringApplication.run(EasyExportDataApplication.class, args);
    }
}
